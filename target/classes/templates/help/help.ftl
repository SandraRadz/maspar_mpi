<#assign title = "Math partner Help">
<#assign nav_active = _("navbar.help")>
<#assign change_lang_link = "../../" + _("navbar.changelang_locale") + "/help/${curr_page_name}">
<#assign ru_link = "../../ru/help/${curr_page_name}">
<#assign en_link = "../../en/help/${curr_page_name}">
<#assign he_link = "../../iw/help/${curr_page_name}">
<#assign ua_link = "../../ua/help/${curr_page_name}">
<#assign am_link = "../../am/help/${curr_page_name}">
<#assign inHelp=true>

<#include "../common/header.ftl">

<#include "../common/top_navbar.ftl">
<div class="container well">
  <div class="loading help"></div>
  <a href="./">${_("help.back_to_toc")}</a>

  ${help_content}

  <a href="./">${_("help.back_to_toc")}</a>
</div>

<#include "../common/mathjax.ftl">
<#include "../common/jsmain.ftl">
<#include "../common/footer.ftl">