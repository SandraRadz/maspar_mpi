/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.Graphic2D.Element2D;

import com.mathpar.Graphic2D.*;
import com.mathpar.func.F;
import com.mathpar.func.Page;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.mathpar.number.*;
import com.mathpar.polynom.Polynom;

/**
 * Класс Planimetria является общим для всех объектов пакета Element2D, т.е.
 * конструкторы print, toString работаеют сразу для нескольких планиметрических
 * объектов. ПРИМЕР p
 * =\paintElement('Многоугольник=вершины(0,0),(0,6),(6,6),(6,0)'); или
 * "Окружность =( центр( 0 , 0 ); радиус (5) ;Сектор=(0, 30); Касательная=(20 ,
 * 10));"; // "Эллипс = центр( 0 , 5 ); полуосьХ 6, полуосьY 2 ; угол( 30
 * );Сектор=(0, 90); Касательная=(90 , 10);"; // "Угол = значение 90, точки( (0,
 * 0) , (5, 0) , (0,0) )"; // "Прямая = (2 , 1);"; // "Отрезок = (начало(0 , 0),
 * конец (10, 10 ) ; деление( 2 : 1)) "; // "Точка = (1,1)"; // "Многоугольник =
 * вершины (0,0),(5,0),(5,5) ... "; // "Обозначения =( А(0 , 0), В(5, 0 ),
 * C(5,5)) "; // "Масштаб = (уменьшить) ";
 *
 */
public class Planimetria {
    public String path = System.getProperty("user.home") + "/image.png";
    public D2Element[] object;
    public F elementF;
    public String title;

    /**
     *
     */
    public Planimetria() {
    }

    /**
     *
     * @param object
     */
    public Planimetria(D2Element[] object) {
        this.object = object;
    }

    /**
     *
     */
    static void clear() {
        DrawElement draw = new DrawElement();
        ToolBar.ClearAll = true;
        draw.removeAllObjects();
    }

    /**
     *
     * @param d
     */
    void print(DrawElement d) {
        for (int i = 0; i < object.length; i++) {
            object[i].create(d);
        }
    }

    /**
     *
     * @param image
     */
    public void saveToFile(BufferedImage image) {
        System.out.println("IMAGE TO FILE");
        OutputStream os = null;
        try {
            try {
                os = new BufferedOutputStream(new FileOutputStream(new File(path)));
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Planimetria.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                javax.imageio.ImageIO.write(image, "PNG", os);
            } catch (IOException ex) {
                Logger.getLogger(Planimetria.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                os.close();
            } catch (IOException ex) {
                Logger.getLogger(Planimetria.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * создание из точки объект - F
     *
     * @param x
     * @param y
     */
    public void createPointF(double x, double y) {
        F fx1 = new F(F.VECTORS, new Element[] {new Polynom(new NumberR64(x))});
        F fy1 = new F(F.VECTORS, new Element[] {new Polynom(new NumberR64(y))});
        F ff1 = new F(F.VECTORS, new Element[] {fx1, fy1});
        elementF = new F(F.TABLEPLOT, new Element[] {ff1});
    }

    /**
     * создание из интервала объект - F
     *
     * @param x
     * @param y
     */
    public void createIntervalF(double x1, double x2, double y1, double y2) {
        //1 line
        F fx1 = new F(F.VECTORS, new Element[] {new Polynom(new NumberR64(x1)), new Polynom(new NumberR64(x2))});
        F fy1 = new F(F.VECTORS, new Element[] {new Polynom(new NumberR64(y1)), new Polynom(new NumberR64(y2))});
        F ff1 = new F(F.VECTORS, new Element[] {fx1, fy1});
        elementF = new F(F.TABLEPLOT, new Element[] {ff1});
    }

    /**
     * создание из многоугольника объект - F
     *
     * @param x
     * @param y
     */
    public void createPolygonF(double[] x, double[] y) {
        //1 line
        F fx1 = new F(F.VECTORS, new Element[] {new Polynom(new NumberR64(x[1])), new Polynom(new NumberR64(x[2]))});
        F fy1 = new F(F.VECTORS, new Element[] {new Polynom(new NumberR64(y[1])), new Polynom(new NumberR64(y[2]))});
        F ff1 = new F(F.VECTORS, new Element[] {fx1, fy1});
        F t1 = new F(F.TABLEPLOT, new Element[] {ff1});
        //2 line
        F fx2 = new F(F.VECTORS, new Element[] {new Polynom(new NumberR64(x[0])), new Polynom(new NumberR64(x[1]))});
        F fy2 = new F(F.VECTORS, new Element[] {new Polynom(new NumberR64(y[0])), new Polynom(new NumberR64(y[1]))});
        F ff2 = new F(F.VECTORS, new Element[] {fx2, fy2});
        F t2 = new F(F.TABLEPLOT, new Element[] {ff2});
        //3 line
        F fx3 = new F(F.VECTORS, new Element[] {new Polynom(new NumberR64(x[2])), new Polynom(new NumberR64(x[3]))});
        F fy3 = new F(F.VECTORS, new Element[] {new Polynom(new NumberR64(y[2])), new Polynom(new NumberR64(y[3]))});
        F ff3 = new F(F.VECTORS, new Element[] {fx3, fy3});
        F t3 = new F(F.TABLEPLOT, new Element[] {ff3});
        //4 line
        F fx4 = new F(F.VECTORS, new Element[] {new Polynom(new NumberR64(x[0])), new Polynom(new NumberR64(x[3]))});
        F fy4 = new F(F.VECTORS, new Element[] {new Polynom(new NumberR64(y[0])), new Polynom(new NumberR64(y[3]))});
        F ff4 = new F(F.VECTORS, new Element[] {fx4, fy4});
        F t4 = new F(F.TABLEPLOT, new Element[] {ff4});
        F arr = new F(F.VECTORS, new Element[] {t1, t2, t3, t4});
        elementF = new F(F.SHOWPLOTS, new Element[] {arr});
    }

    /**
     *
     * @param page
     * @param parcaPort
     * @param str
     */
    public void paint(final Page page, final boolean parcaPort, final String[] str) {
        Ring r = new Ring("R64[]");
        r.setDefaulRing();
        ReadFromFile R = new ReadFromFile(str);
        String t = str[0];
        title = t;
        if (R.nameElement.equals("ТОЧКА ")) {
            double x = ReadFromFile.Xvis;;
            double y = ReadFromFile.Yvis;
            createPointF(x, y);
        }
        if (R.nameElement.equals("ОТРЕЗОК ")) {
            double x1 = ReadFromFile.Xvis;
            double x2 = ReadFromFile.Xvis1;
            double y1 = ReadFromFile.Yvis;
            double y2 = ReadFromFile.Yvis1;
            createIntervalF(x1, x2, y1, y2);
        }
        if (R.nameElement.equals("МНОГОУГОЛЬНИК ")) {
            double[] x = ReadFromFile.pX;
            double[] y = ReadFromFile.pY;
            createPolygonF(x, y);
        }
    }
}
