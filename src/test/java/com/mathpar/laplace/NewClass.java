/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.laplace;

/**
 *
 * @author gennady
 */
 

import static com.mathpar.func.PageTest.doPageTest;

import java.util.LinkedHashMap;
import java.util.Map;
import com.mathpar.number.Ring;
import org.junit.Before;
import org.junit.Test;

import static com.mathpar.func.PageTest.*;
import static com.mathpar.func.PageTest.doPageTest;

/**
 * Tests of simple arithmetics in page.
 */
public class NewClass {
    private Ring r;

    @Before
    public void beforeTest() {r=new Ring("R[t]");
       // r = new Ring("Z[x, y, z]");
    }

  
     @Test
    public void Laplace() {
        doPageTest(r, "  SPACE=R[t];\n" +
"e=0.01;\n" +
"g=\\systLDE(3\\d(x, t)+2x+\\d(y, t)=1, \\d(x, t)+4\\d(y, t)+3y=0);\n" +
"f=\\initCond(\\d(x, t, 0, 0)=0, \\d(x, t, 0, 1)=0, \n" +
"            \\d(y, t, 0, 0)=0, \\d(y, t, 0, 1)=0);\n" +
"h=\\solveLDE(g, f, e);  ", "[(5.5+(-3.3)*\\exp(-0.55t)+(-2.2)*\\exp(-t)),((-2.2)*\\exp(-0.55t)+2.2*\\exp(-t))]");}
 
       @Test
    public void Lape() {
        doPageTest(r,   
                "SPACE=R64[t]; L=\\laplaceTransform(\\exp(3t)); "
                + "\\print(L);", "L=1.0/(t-3.0)");}
 
}