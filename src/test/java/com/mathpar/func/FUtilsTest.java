/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.func;

import com.mathpar.number.Ring;
import org.junit.*;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * @author gennadi
 */
public class FUtilsTest {

    public FUtilsTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testDeleteDoubledBrackets() {
        // System.out.println("deleteDoubledBrackets");
        String inp = "((67))";
        String expResult = "67";
        String result = FUtils.deleteDoubledBrackets(inp);
        assertEquals(expResult, result);
    }

    @Test
    public void testCleanDoubledBrackets() {
        // System.out.println("cleanDoubledBrackets");
        StringBuilder inp = new StringBuilder("(((5-9+7*{8}((ab)))))");
        StringBuilder result = FUtils.cleanDoubledBrackets(inp);
        assertEquals("(5-9+7*{8}ab)", result.toString());
    }

    @Test
    public void testCutByCommas() {
        // System.out.println("cutByCommas");
        String W = "(uiuiu)a,b,c";
        Ring ring = null;
        String[] e = new String[3];
        e[0] = "(uiuiu)a";
        e[1] = "b";
        e[2] = "c";
        String[] result = FUtils.cutByCommas(W, ring);
        assertArrayEquals(e, result);
    }

    @Test
    public void testCutByCommas01() {
        // System.out.println("cutByCommas");
        String W = "(uiuiua,b),c{}";
        Ring ring = null;
        String[] e = new String[2];
        e[0] = "(uiuiua,b)";
        e[1] = "c{}";
        String[] result = FUtils.cutByCommas(W, ring);
        assertArrayEquals(e, result);
    }

    @Test
    public void testCutByCommas02() {
        // System.out.println("cutByCommas");
        String W = "h_{a,b,c}";
        Ring ring = null;
        String[] e = new String[1];
        e[0] = "h_{a,b,c}";
        String[] result = FUtils.cutByCommas(W, ring);
        assertArrayEquals(e, result);
    }
}
